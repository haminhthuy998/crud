<?php
require_once ('dbhelp.php');
?>

<!doctype html>
<html>
<head>
    <title>Quan ly hoc sinh</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
    <div class='container'>
        <div class="panel panel-primany">
            <div class="panel-heading">
                Quản lý thông tin sinh viên
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Họ và Tên</th>
                            <th>Tuổi</th>
                            <th>Địa chỉ</th>
                            <th width="60px"></th>
                            <th width="60px"></th>
                        </tr>
                    </thead>
                    <tbody>
<?php
$sql = 'select * from student';
$students = queryDB($sql);
$index = 1;
foreach ($students as $student){
    echo'<tr>
                        <td>'.($index++).'</td>
                        <td>'.$student['fullname'].'</td>
                        <td>'.$student['age'].'</td>
                        <td>'.$student['address'].'</td>
                        <td><button class="btn btn-warning" onclick=\'window.open("add.php?id='.$student['id'].'","_self")\'>Edit</button></td>
                        <td><button class="btn btn-danger" onclick="deleteStudent('.$student['id'].')">Delete</button></td>
                    </tr>';
}
?>
                    </tr>
                    </tbody>

                </table>
                <button class ="btn btn-success" onclick="window.open('add.php', '_self')">Add Student</button>
            </div>

        </div>
    </div>
</body>
<script type="text/javascript">
    function deleteStudent(id) {
        option = confirm('Bạn có muốn xoá sinh viên này không')
        if(!option) {
            return;
        }

        console.log(id)
        $.post('delete_student.php', {
            'id': id
        }, function(data) {
            alert(data)
            location.reload()
        })
    }
</script>
</html>